# from tasks import <YOUR_TASK>


def old_main():
    import time
    from tasks import add

    add_task = add.delay(1, 2)
    print("add_task.id: {}".format(add_task.id))
    print(add_task.get())

    # OR

    add_task = add.delay(1, 2)
    print("add_task.id: {}".format(add_task.id))
    while add_task.status != "SUCCESS":
        print("add_task.status: {}".format(add_task.status))
        time.sleep(1)
    print("add_task.result: {}".format(add_task.result))


def main():
    # TODO: add your code here
    pass


if __name__ == "__main__":
    main()
