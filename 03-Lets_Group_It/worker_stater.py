from our_celery_app import app
import tasks

argv = ["worker", "--loglevel", "info", "--concurrency", "3"]
app.worker_main(argv)
